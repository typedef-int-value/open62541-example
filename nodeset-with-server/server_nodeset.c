/* This work is licensed under a Creative Commons CCZero 1.0 Universal License.
 * See http://creativecommons.org/publicdomain/zero/1.0/ for more information. */

#include "open62541.h"

#include "DoorLockType.h"

UA_Boolean running = true;

int main(int argc, char** argv) {

    UA_ServerConfig *config = UA_ServerConfig_new_default();
    UA_Server *server = UA_Server_new(config);

    UA_StatusCode retval;

    /* create nodes from DoorLockType */
    if(DoorLockType(server) != UA_STATUSCODE_GOOD) {
        UA_LOG_ERROR(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "Could not add the nodeset. "
        "Check previous output for any error.");
        retval = UA_STATUSCODE_BADUNEXPECTEDERROR;
    } else {
        UA_NodeId createdNodeId;
        UA_ObjectAttributes object_attr = UA_ObjectAttributes_default;
        object_attr.description = UA_LOCALIZEDTEXT("en-US", "Discription of DoorLock");
        object_attr.displayName = UA_LOCALIZEDTEXT("en-US", "Doorlock");

        // this will just get the namespace index, since it is already added to the server
        UA_UInt16 nsIdx = UA_Server_addNamespace(server, "http://skuniv.ac.kr/pin");
        UA_NodeId assignedID;
        UA_Server_addObjectNode(server, UA_NODEID_NULL,
                                UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                                UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                UA_QUALIFIEDNAME(nsIdx, "Doorlock"), 
                                UA_NODEID_NUMERIC(nsIdx, 2001),
                                object_attr, NULL, &assignedID);

        retval = UA_Server_run(server, &running);
    }

    UA_Server_delete(server);
    UA_ServerConfig_delete(config);

    return (int)retval;
}
